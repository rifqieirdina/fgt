# net config server /hidden:yes /srvcomment:"Windows Server" > out.txt 2 >&1
# net user irdina Rame@123 /add /active:yes /logonpasswordchg:no > nul
$Password = ConvertTo-SecureString "Rame@123" -AsPlainText -Force
New-LocalUser -Name "irdina" -Password $Password -PasswordNeverExpires:$true -UserMayNotChangePassword:$true -AccountNeverExpires:$true
# Enable-LocalUser -Name "irdina"

# net localgroup Administrators brooke /add > nul
Add-LocalGroupMember -Group "Administrators" -Member "irdina"

# diskperf -y > nul
# Enable-DiskPerf -DriveLetter * -Counters * -Force

# sc config audiosrv start= auto > nul
Set-Service -Name "audiosrv" -StartupType Automatic

# sc start audiosrv > nul
Start-Service -Name "audiosrv"

# icacls C:\Windows\Temp /grant irdina:F > nul
# Add-Acl -Path "C:\Windows\Temp" -Account "irdina" -AccessRights "FullControl"
# icacls C:\Windows\installer /grant irdina:F > nul
# Add-Acl -Path "C:\Windows\installer" -Account "irdina" -AccessRights "FullControl"

# echo IP:
Write-Output "Server name: "

# Start-Process cmd -ArgumentList " tasklist | find /i 'ngrok.exe' > nul && curl -s localhost:4040/api/tunnels | jq -r .tunnels[0].public_url || echo 'Failed to retreive NGROK authtoken - check again your authtoken' "
$response = Invoke-WebRequest -Uri "http://localhost:4040/api/tunnels" -Method GET -UseBasicParsing
$responseJson = $response.Content | ConvertFrom-Json
$url = $responseJson.tunnels[0].public_url

$uri = New-Object System.Uri($url)
$port = $uri.Port
$hostname = $uri.Host
Write-Host ${hostname}:${port}

# if (Get-Process ngrok -ErrorAction SilentlyContinue) {
#     $response = Invoke-WebRequest -Uri "http://localhost:4040/api/tunnels" -Method GET -UseBasicParsing
#     $responseJson = $response.Content | ConvertFrom-Json
#     $responseJson.tunnels[0].public_url
# } else {
#     "Failed to retreive NGROK authtoken - check again your authtoken"
# }

# Test-Connection -Count 10 -ComputerName "127.0.0.1"